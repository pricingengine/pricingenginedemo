package com.example.social.service;

import com.example.social.domain.Seat;
import com.example.social.repository.SeatRepository;

public class SeatService {


	private Long date = null;

	private Seat seat = null;

	private SeatRepository seatRepository = null;

	public SeatService(Long date, Seat seat) {
		this.date = date;
		this.seat = seat;
	}

	public double getPrice() {
		return seat.getPrice();
	}

	public void calculatePrice() {
		double price = seatRepository.getSeatPrice(seat.getType(), date);
		double totalPrice = price;
		seat.setPrice(totalPrice);
	}
	
}
