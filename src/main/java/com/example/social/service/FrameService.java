package com.example.social.service;

import org.springframework.stereotype.Service;

import com.example.social.domain.Frame;
import com.example.social.repository.FrameRepository;


public class FrameService {

		private Long date = null;
		private Frame frame = null;

		private FrameRepository frameRepository = null;

		public FrameService(Long date, Frame wheel) {
			this.date = date;
			this.frame = wheel;

		}

		public double getPrice() {
			return frame.getPrice();
		}

		public void calculatePrice() {
			double spokePrice = frameRepository.getFramePrice(frame.getType(), date);
			double totalPrice = spokePrice;
			frame.setPrice(totalPrice);
		}


}
