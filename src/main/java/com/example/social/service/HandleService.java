package com.example.social.service;

import com.example.social.domain.Handle;
import com.example.social.repository.HandleRepository;

public class HandleService {

	private Long date = null;

	private Handle handle = null;

	private HandleRepository handleRepository = null;

	public HandleService(Long date, Handle handle ) {
		this.date = date;
		this.handle = handle;
		this.handleRepository = handleRepository;
	}

	public double getPrice() {
		return handle.getPrice();
	}

	public void calculatePrice() {
		double spokePrice = handleRepository.getHandlePrice(handle.getType(), date);

		double shockLockPrice = handleRepository.getShocklockPrice(handle.isShockLock(), date);
		double totalPrice = spokePrice + shockLockPrice;
		handle.setPrice(totalPrice);
	}

}
