package com.example.social.service;

import com.example.social.domain.Wheel;
import com.example.social.repository.WheelRepository;

public class WheelService {

	private Long date = null;

	private Wheel wheel = null;

	private WheelRepository wheelRepository = null;

	public WheelService(Long date, Wheel wheel) {
		this.date = date;
		this.wheel = wheel;
		this.wheelRepository = wheelRepository;
	}

	public double getPrice() {
		return wheel.getPrice(); 
	}

	public void calculatePrice() {

		double spokePrice = wheelRepository.getSpokePrice(wheel.getSpoke(), date);

		double rimPrice = wheelRepository.getRimPrice(wheel.getRim(), date);

		double tubePrice = wheelRepository.getTubePrice(wheel.getRim(), date);

		double tyrePrice = wheelRepository.getTyrePrice(wheel.getRim(), date);

		double totalPrice = spokePrice + rimPrice + tubePrice + tyrePrice;

		wheel.setPrice(totalPrice);

	}

}


