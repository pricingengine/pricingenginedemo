package com.example.social.service;

import com.example.social.domain.ChainAssembly;
import com.example.social.repository.ChainAssemblyRepository;

public class ChainAssemblyService {
	
	private Long date = null;

	private ChainAssembly chainAssembly = null ;

	private ChainAssemblyRepository chainAssemblyRepository = null;

	public ChainAssemblyService(Long date, ChainAssembly chainAssembly) {
		this.date = date;
		this.chainAssembly = chainAssembly;
	}

	public double getPrice() {
		return chainAssembly.getPrice();
	}

	public void calculatePrice() {
		double sizePrice = chainAssemblyRepository.getChainSizePrice(chainAssembly.getChainSize(), date);

		double speedPrice = chainAssemblyRepository.getChainSpeedPrice(chainAssembly.getSpeed(), date);

		double totalPrice = sizePrice + speedPrice;
		chainAssembly.setPrice(totalPrice);
	}

}
