package com.example.social.enumuration;

public enum FrameTypes {

	CANTILEVER,DIAMOND,ORDINARY,PRONE,RECUMBENT,STEPTHROUGH;
}
