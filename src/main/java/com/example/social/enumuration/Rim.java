package com.example.social.enumuration;

public enum Rim {

	STEEL, ALLOY, CHROME
}
