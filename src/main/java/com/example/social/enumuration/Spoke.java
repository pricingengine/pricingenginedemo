package com.example.social.enumuration;

public enum Spoke {

	TANGENTIALLACING, WHEELBUILDING;
}
