package com.example.social.price;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import com.example.social.domain.Cycle;
import com.example.social.service.ChainAssemblyService;
import com.example.social.service.FrameService;
import com.example.social.service.HandleService;
import com.example.social.service.SeatService;
import com.example.social.service.WheelService;

public class PriceCalculator implements Callable<Cycle> {

	Cycle cycle;

	public PriceCalculator(Cycle cycle) {
		this.cycle = cycle;
	}


	@Override
	public Cycle call() throws Exception {

		String date = cycle.getDate();
		Long epochDate = getEpochTime(date);

		WheelService wheelService = new WheelService(epochDate, cycle.getWheel());
		wheelService.calculatePrice();

		FrameService frameService = new FrameService(epochDate, cycle.getFrame());
		frameService.calculatePrice();

		HandleService handleService = new HandleService(epochDate, cycle.getHandle());
		handleService.calculatePrice();

		ChainAssemblyService chainAssemblyService = new ChainAssemblyService(epochDate, cycle.getChainAssembly());
		chainAssemblyService.calculatePrice();

		SeatService seatService = new SeatService(epochDate, cycle.getSeat());
		seatService.calculatePrice();

		cycle.calculatePrice();
		
		return cycle;
	}

	private Long getEpochTime(String dateInString) {
		Long epochTime = Long.valueOf(0);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date inputDate = formatter.parse(dateInString);
			epochTime = inputDate.getTime() / 1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return epochTime;
	}

}